import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {SparkyHttpService} from "../common/sparky-http.service";
import {TagStat} from "./tagstat";
import {RestError} from "../common/rest-error";
import {PostStat} from "./poststat";

@Injectable()
export class StatsService {
    private apiUrl = "http://localhost:8080";  // FIXME: inject config into services

    constructor(private http: SparkyHttpService) {
    }

    fetchPostStats(): Observable<PostStat[]> {
        return this.http
            .get(this.apiUrl + "/stats/posts")
            .map(this.mapToPostStats)
            .catch(this.handleError);
    }

    private mapToPostStats(res: Response) {
        let postStatsAsJsonArray = res.json();
        return postStatsAsJsonArray
            ? postStatsAsJsonArray.postStats.map(function(postStatAsJson) { return PostStat.createFrom(postStatAsJson); })
            : {};
    }

    fetchAllTagStats(): Observable<TagStat[]> {
        return this.http
            .get(this.apiUrl + "/stats/tags")
            .map(this.mapToTagStats)
            .catch(this.handleError);
    }


    fetchSingleTagStats(tag: string): Observable<TagStat[]> {
        return this.http
            .get(this.apiUrl + "/stats/tags/" + tag)
            .map(this.mapToTagStats)
            .catch(this.handleError);
    }

    private mapToTagStats(res: Response) {
        let tagStatsAsJsonArray = res.json();
        return tagStatsAsJsonArray
            ? tagStatsAsJsonArray.tags.map(function(tagStatAsJson) { return TagStat.createFrom(tagStatAsJson); })
            : {};
    }

    private handleError(error: Response | any) {
        console.error(error);
        return Observable.throw(
            new RestError(
                error.status,
                error.json().reason
            )
        );
    }
}