export class PostStat {
    constructor(readonly month: string, readonly numOfPosts) {
    }

    public static createFrom(postStatAsJson: any) {
        return new PostStat(
            postStatAsJson.month,
            postStatAsJson.numOfPosts
        );
    }
}