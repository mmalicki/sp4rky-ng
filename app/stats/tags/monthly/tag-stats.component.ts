import {Component, OnInit, Input, OnChanges, SimpleChanges} from "@angular/core";
import {StatsService} from "../../stats.service";
import {TagStat} from "../../tagstat";
import {ERROR_MESSAGES} from "../../../common/messages";
import {RestError} from "../../../common/rest-error";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
    moduleId: module.id,
    selector: 'tag-stats',
    templateUrl: 'tag-stats.html'
})

export class TagStatsComponent implements OnInit, OnChanges {
    tagStats: TagStat[];
    errorMessage: string;

    @Input()
    chosenMonth: number = new Date().getMonth();
    @Input()
    chartHeight: string = '400px';
    @Input()
    type: string = 'tags-popularity'; // tags-popularity albo single-tag (wtedy musi być podany tag)
    @Input()
    tag: string;
    @Input()
    titleConf: any = {};

    sanitizedChartHeight;

    public barChartLabels: string[] = [];
    public barChartData: any[] = [{label: 'Liczba postów z wybranym tagiem', data: [], fillColor: 'blue'}];
    public barChartType: string = 'bar';
    public barChartLegend: boolean = true;
    public barChartBarColor: Array<any> = [
        {
            backgroundColor: '#dadada',
            borderColor: '#9c9c9c'
        }
    ];
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                }
            }]
        },
        title: this.titleConf
    };

    constructor(private statsService: StatsService, private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        this.fetch();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.sanitizedChartHeight = this.sanitizer.bypassSecurityTrustStyle(this.chartHeight + ' !important');
        this.barChartOptions.title = this.titleConf;
        this.fetch();
    }

    onChosenMonthChange() {
        this.fetch();
    }

    fetch(): void {
        if(this.type === 'tags-popularity') {
            this.statsService.fetchAllTagStats()
                .subscribe(
                    tagStats => {
                        this.tagStats = tagStats;
                        this.prepareChartData();
                    },
                    error => this.errorMessage = this.mapErrorToMessage(error)
                );
        } else if (this.type === 'single-tag') {
            this.statsService.fetchSingleTagStats(this.tag)
                .subscribe(
                    tagStats => {
                        this.tagStats = tagStats;
                        this.prepareChartData();
                    },
                    error => this.errorMessage = this.mapErrorToMessage(error)
                );
        }
    }

    private prepareChartData() {
        this.barChartLabels = [];
        this.barChartData[0].data = [];
        if(this.type === 'tags-popularity') {
            this.prepareDataForTagPopularity();
        } else if(this.type === 'single-tag') {
            this.prepareDataForSingleTag();
        } else {
            console.log('unknown chart type');
        }
    }

    private prepareDataForTagPopularity() {
        for (let stat of this.tagStats) {
            this.barChartLabels.push('#' + stat.tagName);

            let numOfPosts = 0;
            for(let postsInMonth of stat.numOfPostsInMonth) {
                if(postsInMonth.month === this.chosenMonth) {
                    numOfPosts = postsInMonth.numOfPosts;
                }
            }
            this.barChartData[0].data.push(numOfPosts);
        }
    }

    private prepareDataForSingleTag() {
        let tagStat: TagStat;
        for (let stat of this.tagStats) {
            if (stat.tagName === this.tag) {
                tagStat = stat;
            }
        }
        for (let stat of tagStat.numOfPostsInMonth) {
            this.barChartLabels.push(stat.month);
            this.barChartData[0].data.push(stat.numOfPosts);
        }
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}