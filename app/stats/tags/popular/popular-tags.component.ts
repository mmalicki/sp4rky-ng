import {Component, Injectable, OnInit} from "@angular/core";
import {TagStat} from "../../tagstat";
import {StatsService} from "../../stats.service";
import {RestError} from "../../../common/rest-error";
import {ERROR_MESSAGES} from "../../../common/messages";

@Component({
    moduleId: module.id,
    selector: 'popular-tags',
    templateUrl: 'popular-tags.html'
})

@Injectable()
export class PopularTagsComponent implements OnInit {
    errorMessage: string;
    tagStats: TagStat[];

    constructor(private statsService: StatsService) {
    }

    ngOnInit() {
        this.fetch();
    }

    fetch(): void {
        this.statsService.fetchAllTagStats()
            .subscribe(
                tagStats => this.tagStats = tagStats,
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}