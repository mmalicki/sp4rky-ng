export class TagStat {
    constructor(readonly tagName: string, readonly numOfPostsInMonth) {
    }

    public static createFrom(tagStatAsJson: any) {
        return new TagStat(
            tagStatAsJson.tag,
            tagStatAsJson.numOfPostsInMonth
        );
    }
}