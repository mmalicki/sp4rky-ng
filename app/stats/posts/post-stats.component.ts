import {Component, OnInit, Input, OnChanges, SimpleChanges} from "@angular/core";
import {StatsService} from "../stats.service";
import {ERROR_MESSAGES} from "../../common/messages";
import {RestError} from "../../common/rest-error";
import {DomSanitizer} from "@angular/platform-browser";
import {PostStat} from "../poststat";

@Component({
    moduleId: module.id,
    selector: 'post-stats',
    templateUrl: 'post-stats.html'
})

export class PostStatsComponent implements OnInit, OnChanges {
    postStats: PostStat[];
    errorMessage: string;

    @Input()
    chartHeight: string = '400px';
    @Input()
    tag: string;
    @Input()
    titleConf: any = {};

    sanitizedChartHeight;

    public barChartLabels: string[] = [];
    public barChartData: any[] = [{label: 'Liczba postów w wybranym miesiącu', data: [], fillColor: 'blue'}];
    public barChartType: string = 'bar';
    public barChartLegend: boolean = true;
    public barChartBarColor: Array<any> = [
        {
            backgroundColor: '#dadada',
            borderColor: '#9c9c9c'
        }
    ];
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                }
            }]
        },
        title: this.titleConf
    };

    constructor(private statsService: StatsService, private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        this.fetch();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.sanitizedChartHeight = this.sanitizer.bypassSecurityTrustStyle(this.chartHeight + ' !important');
        this.barChartOptions.title = this.titleConf;
        this.fetch();
    }

    fetch(): void {
        this.statsService.fetchPostStats()
            .subscribe(
                postStats => {
                    this.postStats = postStats;
                    this.prepareChartData();
                },
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    private prepareChartData() {
        this.barChartLabels = [];
        this.barChartData[0].data = [];
        for (let stat of this.postStats) {
            this.barChartLabels.push(stat.month);
            this.barChartData[0].data.push(stat.numOfPosts);
        }
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}