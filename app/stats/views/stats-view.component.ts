import {Component} from "@angular/core";
import * as moment from 'moment';

@Component({
    moduleId: module.id,
    selector: 'stats-view',
    templateUrl: 'stats-view.html'
})

export class StatsViewComponent {
    months: String[] = [];
    chosenMonth: String;

    constructor() {
        this.months = [];
        for(var monthsToSubtract = 12; monthsToSubtract >= 0; monthsToSubtract--) {
            this.months.push(moment().subtract(monthsToSubtract, 'months').format("MM-YYYY"));
        }
        this.chosenMonth = this.months[this.months.length - 1];
    }
}