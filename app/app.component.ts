import {Component, OnInit} from "@angular/core";
import {AuthService} from "./users/service/auth.service";
import {Md5} from "ts-md5/dist/md5";
import './rxjs-operators';

@Component({
    moduleId: module.id,
    selector: 'sparky-app',
    templateUrl: 'app.html',
    styleUrls: ['app.css']
})

export class AppComponent implements OnInit {
    avatarPath: string;

    constructor(private authService: AuthService) {
    }

    ngOnInit() {
        if (this.authService.isLogged()) {
            let username = this.authService.getUsername();
            if(username) {
                this.avatarPath = "http://www.gravatar.com/avatar/" + Md5.hashStr(username) + "?s=32&d=identicon&r=PG";
            } else {
                this.avatarPath = "assets/avatar.png";
            }
        }
    }
}