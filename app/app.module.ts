import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {AppComponent} from "./app.component";
import {AppRoutingModule} from "./routing.module";
import {SignupComponent} from "./users/signup/signup.component";
import {UserService} from "./users/service/user.service";
import {SignupOkComponent} from "./users/signup/signup-ok.component";
import {PostService} from "./posts/service/post.service";
import {PostsWallComponent} from "./posts/view/wall/posts-wall.component";
import {PostComponent} from "./posts/view/post.component";
import {SinglePostComponent} from "./posts/view/single/single-post.component";
import {NewPostComponent} from "./posts/new/new-post.component";
import {NewCommentComponent} from "./comments/new/new-comment.component";
import {CommentsListComponent} from "./comments/list/comments-list.component";
import {HttpModule, RequestOptions, XHRBackend} from "@angular/http";
import {SigninComponent} from "./users/signin/signin.component";
import {AuthService} from "./users/service/auth.service";
import {SparkyHttpService} from "./common/sparky-http.service";
import {SignoutComponent} from "./users/signout/signout.component";
import {UserPostsComponent} from "./users/user/user.component";
import {HashTagLinkCreator} from "./posts/tags/tag-link-enhancer";
import {PostsByTagComponent} from "./posts/view/bytag/posts-by-tag.component";
import {EditPostComponent} from "./posts/edit/edit-post.component";
import {PopularTagsComponent} from "./stats/tags/popular/popular-tags.component";
import {ChartsModule} from "ng2-charts";
import {StatsService} from "./stats/stats.service";
import {TagStatsComponent} from "./stats/tags/monthly/tag-stats.component";
import {StatsViewComponent} from "./stats/views/stats-view.component";
import {EditCommentComponent} from "./comments/edit/edit-comment.component";
import {PostStatsComponent} from "./stats/posts/post-stats.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        ChartsModule
    ],
    declarations: [
        AppComponent,
        PostComponent,
        NewPostComponent,
        EditPostComponent,
        NewCommentComponent,
        CommentsListComponent,
        SinglePostComponent,
        EditCommentComponent,
        PostsWallComponent,
        SignupComponent,
        SignupOkComponent,
        SignupComponent,
        SigninComponent,
        SignoutComponent,
        UserPostsComponent,
        HashTagLinkCreator,
        PostsByTagComponent,
        PopularTagsComponent,
        TagStatsComponent,
        StatsViewComponent,
        PostStatsComponent
    ],
    providers: [
        UserService,
        PostService,
        UserService,
        StatsService,
        AuthService,
        {
            provide: SparkyHttpService,
            useFactory: (backend: XHRBackend, options: RequestOptions) => {
                return new SparkyHttpService(backend, options);
            },
            deps: [XHRBackend, RequestOptions],
        }

    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule {
}