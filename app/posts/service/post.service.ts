import {Injectable} from "@angular/core";
import {Response, URLSearchParams} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {RestError} from "../../common/rest-error";
import {Post} from "../model/post";
import {Comment} from "../../comments/comment";
import {NewPostRequest} from "../new/new-post-request";
import {NewCommentRequest} from "../../comments/new/new-comment-request";
import {SparkyHttpService} from "../../common/sparky-http.service";
import {EditPostRequest} from "../edit/edit-post-request";
import {EditCommentRequest} from "../../comments/edit/edit-comment-request";

@Injectable()
export class PostService {
    private apiUrl = "http://localhost:8080";  // FIXME: inject config into services

    constructor(private http: SparkyHttpService) {
    }

    fetch(postId: number): Observable<Post> {
        return this.http
            .get(this.apiUrl + "/posts/" + postId)   // FIXME: adres na pewno można elegancko podać np. localhost/posts/{postId} i wstrzyknąć wartość
            .map(this.mapToPost)
            .catch(this.handleError);
    }

    fetchAll(): Observable<Post[]> {
        return this.http
            .get(this.apiUrl + "/posts")
            .map(this.mapToPosts)
            .catch(this.handleError);
    }

    fetchAllByUser(username: string): Observable<Post[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('user', username);

        console.log(params);

        return this.http
            .get(this.apiUrl + "/posts", {search: params})
            .map(this.mapToPosts)
            .catch(this.handleError);
    }

    fetchAllByTag(tag: string): Observable<Post[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('tag', tag);

        return this.http
            .get(this.apiUrl + "/posts", {search: params})
            .map(this.mapToPosts)
            .catch(this.handleError);
    }

    createPost(newPostRequest: NewPostRequest): Observable<Post> {
        return this.http
            .post(this.apiUrl + "/posts", JSON.stringify(newPostRequest))
            .map(this.mapToPost)
            .catch(this.handleError);
    }

    editPost(postId: number, editPostRequest: EditPostRequest): Observable<Post> {
        return this.http
            .put(this.apiUrl + "/posts/" + postId, JSON.stringify(editPostRequest))
            .map(this.mapToPost)
            .catch(this.handleError);
    }

    deletePost(postId: number): Observable<Boolean> {
        return this.http
            .delete(this.apiUrl + "/posts/" + postId)
            .map(x => {
            })
            .catch(this.handleError);
    }

    fetchAllComments(postId: number): Observable<Comment[]> {
        return this.http
            .get(this.apiUrl + "/posts/" + postId + "/comments")
            .map(this.mapToComments)
            .catch(this.handleError);
    }

    editComment(commentId: number, editCommentRequest: EditCommentRequest) {
        console.log(commentId);
        return this.http
            .put(this.apiUrl + "/comments/" + commentId, JSON.stringify(editCommentRequest))
            .map(this.mapToComment)
            .catch(this.handleError);
    }

    fetchAllCommentsByUser(name: string) {
        return this.http
            .get(this.apiUrl + "/users/" + name + " /comments")
            .map(this.mapToComments)
            .catch(this.handleError);
    }

    createComment(postId: number, newCommentRequest: NewCommentRequest): Observable<Post> {
        return this.http
            .post(this.apiUrl + "/posts/" + postId + "/comments", JSON.stringify(newCommentRequest))
            .map(this.mapToComment)
            .catch(this.handleError);
    }

    private mapToPosts(res: Response) {
        let postsAsJsonArray = res.json();
        return postsAsJsonArray
            ? postsAsJsonArray.map(function (postAsJson) {
            return Post.createFrom(postAsJson);
        })
            : {};
    }

    private mapToPost(res: Response) {
        let postAsJson = res.json();
        return postAsJson ? Post.createFrom(postAsJson) : {};
    }

    private mapToComment(res: Response) {
        let commentAsJson = res.json();
        return Comment.createFrom(commentAsJson);
    }

    private mapToComments(res: Response) {
        let commentsAsJsonArray = res.json();
        return commentsAsJsonArray
            ? commentsAsJsonArray.map(function (commentAsJson) {
            return Comment.createFrom(commentAsJson);
        })
            : {};
    }

    private handleError(error: Response | any) {
        console.error(error);
        return Observable.throw(
            new RestError(
                error.status,
                error.json().reason
            )
        );
    }
}