import {Component, Injectable, OnInit} from "@angular/core";
import {RestError} from "../../../common/rest-error";
import {Post} from "../../model/post";
import {PostService} from "../../service/post.service";
import {AuthService} from "../../../users/service/auth.service";
import {ERROR_MESSAGES} from "../../../common/messages";

@Component({
    moduleId: module.id,
    selector: 'posts-wall',
    templateUrl: 'posts-wall.html'
})

@Injectable()
export class PostsWallComponent implements OnInit {
    posts: Post[];
    errorMessage: string;

    constructor(private postService: PostService, private authService: AuthService) {
    }

    ngOnInit() {
        this.fetchAll();
    }

    shouldDisplayNewPostForm(): boolean {
        return this.authService.isLogged();
    }

    fetchAll(): void {
        console.log("fetch all");
        this.postService.fetchAll()
            .subscribe(
                posts => this.posts = posts,
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}