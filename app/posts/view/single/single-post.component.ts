import {Component, Injectable, OnInit, OnChanges, SimpleChanges} from "@angular/core";
import {RestError} from "../../../common/rest-error";
import {Post} from "../../model/post";
import {PostService} from "../../service/post.service";
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "../../../users/service/auth.service";
import {ERROR_MESSAGES} from "../../../common/messages";


@Component({
    moduleId: module.id,
    selector: 'post-single',
    templateUrl: 'single-post.html'
})

@Injectable()
export class SinglePostComponent implements OnInit {

    post: Post;
    errorMessage: string;

    constructor(private postService: PostService, private route: ActivatedRoute, private authService: AuthService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => this.fetch(params['id']));
    }

    onNewComment(n: boolean) {
        this.route.params.subscribe(params => this.fetch(params['id']));
    }

    fetch(postToFetchId: number): void {
        this.postService.fetch(postToFetchId)
            .subscribe(
                post => this.post = post,
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    shouldDisplayNewCommentForm(): boolean {
        return this.authService.isLogged();
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}