import {Component, Injectable, OnInit} from "@angular/core";
import {RestError} from "../../../common/rest-error";
import {Post} from "../../model/post";
import {PostService} from "../../service/post.service";
import {AuthService} from "../../../users/service/auth.service";
import {ActivatedRoute, Router, Params} from "@angular/router";
import {ERROR_MESSAGES} from "../../../common/messages";


@Component({
    moduleId: module.id,
    selector: 'posts-by-tag',
    templateUrl: 'posts-by-tag.html'
})

@Injectable()
export class PostsByTagComponent implements OnInit {
    posts: Post[];
    errorMessage: string;
    tag: string;

    constructor(private postService: PostService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => this.tag = params['tag']);
        this.fetchAllByTag(this.tag);
    }

    fetchAllByTag(tag: string): void {
        this.postService.fetchAllByTag(tag)
            .subscribe(
                posts => this.posts = posts,
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}