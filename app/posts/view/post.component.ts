import {Component, Injectable, OnInit, Input, OnChanges} from "@angular/core";
import {Post} from "../model/post";
import {AuthService} from "../../users/service/auth.service";
import {UserService} from "../../users/service/user.service";
import {PostService} from "../service/post.service";
import {ERROR_MESSAGES} from "../../common/messages";
import {RestError} from "../../common/rest-error";
import {Router} from "@angular/router";
@Component({
    moduleId: module.id,
    selector: 'post',
    templateUrl: 'post.html'
})

@Injectable()
export class PostComponent {
    private authService: AuthService;
    private postService: PostService;
    private userService: UserService;
    private router: Router;
    @Input()
    post: Post;
    @Input()
    shouldDisplayCommentLink: boolean = true;
    @Input()
    shouldDisplayAsTiles: boolean = false;

    isImageModalVisible: boolean = false;
    isInEditMode: boolean = false;
    errorMessage: string;

    constructor(authService: AuthService, userService: UserService, postService: PostService, router: Router) {
        this.authService = authService;
        this.userService = userService;
        this.postService = postService;
        this.router = router;
    }

    markForEdit() {
        this.isInEditMode = true;
    }

    deletePost() {
        this.postService.deletePost(this.post.id)
            .subscribe(
                success => {
                    this.router.navigate(['wall']);
                },
                error => {this.errorMessage = this.mapErrorToMessage(error)}
            );
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }

    isAllowedToEdit() {
        return this.authService.getUsername() === this.post.user.name || this.authService.getRole() === "ADMIN";
    }

    isAllowedToDelete() {
        return this.authService.getRole() === "ADMIN";
    }

    hideImageModal() {
        this.isImageModalVisible = false;
    }

    showImageModal() {
        this.isImageModalVisible = true;
    }
}
