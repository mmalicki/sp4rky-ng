import {Component, Injectable, Input, Output, EventEmitter, OnInit} from "@angular/core";
import {RestError} from "../../common/rest-error";
import {Router} from "@angular/router";
import {PostService} from "../service/post.service";
import {ERROR_MESSAGES} from "../../common/messages";
import {EditPostRequest} from "./edit-post-request";
import {Post} from "../model/post";

@Component({
    moduleId: module.id,
    selector: 'edit-post-form',
    templateUrl: 'edit-post-form.html'
})

@Injectable()
export class EditPostComponent implements OnInit{

    editPostRequest = new EditPostRequest();
    errorMessage: string;
    @Input()
    post: Post;

    @Output() onSavedPost = new EventEmitter<string>();

    constructor(private postService: PostService, private router: Router) {
    }

    ngOnInit(): void {
        this.editPostRequest.message = this.post.message;
        this.editPostRequest.imagePath = this.post.imagePath;
    }

    editPost(): void {
        this.postService.editPost(this.post.id, this.editPostRequest)
            .subscribe(
                post => {
                    this.onSavedPost.emit("savedPost");
                    this.router.navigate(['/']);
                },
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}