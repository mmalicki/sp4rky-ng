import {Pipe, PipeTransform} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";

@Pipe({name: 'addhashtaglinks'})
export class HashTagLinkCreator implements PipeTransform {
    transform(value: string): string {
        return value == undefined
            ? value
            : value.replace(new RegExp('#([A-Za-z0-9-_]+)', 'g'), '<a href="/tag/$1/posts">#$1</a>').toString();
    }
}
