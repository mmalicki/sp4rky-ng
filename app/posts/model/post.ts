import {User} from "../../users/model/user";
export class Post {
    constructor(readonly id: number, readonly user: User, readonly postDate: string, readonly message: string, readonly imagePath: string) {
    }

    public static createFrom(postAsJson: any) {
        return new Post(
            postAsJson.id,
            User.createFrom(postAsJson.user),
            postAsJson.postDate,
            postAsJson.message,
            !postAsJson.imagePath ? 'assets/no-image.png' : postAsJson.imagePath
        );
    }
}