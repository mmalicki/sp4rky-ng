import {Component, Injectable} from "@angular/core";
import {RestError} from "../../common/rest-error";
import {NewPostRequest} from "./new-post-request";
import {Router} from "@angular/router";
import {PostService} from "../service/post.service";
import {ERROR_MESSAGES} from "../../common/messages";

@Component({
    moduleId: module.id,
    selector: 'new-post-form',
    templateUrl: 'new-post-form.html'
})

@Injectable()
export class NewPostComponent {
    newPostRequest = new NewPostRequest();
    errorMessage: string;

    constructor(private postService: PostService, private router: Router) {
    }

    createPost(): void {
        this.postService.createPost(this.newPostRequest)
            .subscribe(
                post => this.router.navigate(['posts/', post.id]),
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}