import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {SignupComponent} from "./users/signup/signup.component";
import {SignupOkComponent} from "./users/signup/signup-ok.component";
import {PostsWallComponent} from "./posts/view/wall/posts-wall.component";
import {SinglePostComponent} from "./posts/view/single/single-post.component";
import {SigninComponent} from "./users/signin/signin.component";
import {SignoutComponent} from "./users/signout/signout.component";
import {UserPostsComponent} from "./users/user/user.component";
import {PostsByTagComponent} from "./posts/view/bytag/posts-by-tag.component";
import {StatsViewComponent} from "./stats/views/stats-view.component";

const routes: Routes = [
    {path: '', redirectTo: '/wall', pathMatch: 'full'},
    {path: 'wall', component: PostsWallComponent},
    {path: 'posts/:id', component: SinglePostComponent},
    {path: 'signup', component: SignupComponent},
    {path: 'signin', component: SigninComponent},
    {path: 'signup-ok', component: SignupOkComponent},
    {path: 'signout', component: SignoutComponent},
    {path: 'user/:name', component: UserPostsComponent},
    {path: 'tag/:tag/posts', component: PostsByTagComponent},
    {path: 'stats', component: StatsViewComponent},
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}