import {Md5} from "ts-md5/dist/md5";
export class User {
    constructor(readonly name: string, readonly email: string, readonly signupDate: string, readonly role: string, public avatarPath: string ) {
    }

    public static createFrom(userJson: any) {
        return new User(
            userJson.name,
            userJson.email,
            userJson.signupDate,
            userJson.role,
            !userJson.avatarPath ? "http://www.gravatar.com/avatar/" + Md5.hashStr(userJson.name) + "?s=32&d=identicon&r=PG" : userJson.avatarPath
        );
    }
}