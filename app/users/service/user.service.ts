import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import {UserSignupRequestForm} from "../signup/user-signup-request-form";
import {User} from "../model/user";
import {Observable} from "rxjs/Observable";
import {RestError} from "../../common/rest-error";
import {SparkyHttpService} from "../../common/sparky-http.service";

@Injectable()
export class UserService {
    private apiUrl = "http://localhost:8080/users";  // FIXME: inject config into services

    constructor(private http: SparkyHttpService) {
    }

    signup(signupRequest: UserSignupRequestForm): Observable<User> {
        return this.http
            .post(this.apiUrl, JSON.stringify(signupRequest))
            .map(this.mapToObject)
            .catch(this.handleError);
    }

    getUser(userName: string): Observable<User> {
        return this.http
            .get(this.apiUrl + "/" + userName)
            .map(this.mapToObject)
            .catch(this.handleError);
    }

    private mapToObject(res: Response) {
        let userJson = res.json();
        return userJson ? User.createFrom(userJson) : {};
    }

    private handleError(error: any) {
        console.error(error);
        return Observable.throw(
            new RestError(
                error.status,
                error.json().reason
            )
        );
    }
}