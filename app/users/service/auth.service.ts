import {SigninRequest} from "../signin/signin-request";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Response} from "@angular/http";
import {RestError} from "../../common/rest-error";
import {SparkyHttpService} from "../../common/sparky-http.service";

@Injectable()
export class AuthService {
    private loginUrl = "http://localhost:8080/auth-requests"

    constructor(private http: SparkyHttpService) {
    }

    signin(signinRequest: SigninRequest) {
        return this.http
            .post(this.loginUrl, JSON.stringify(signinRequest))
            .map(this.toToken)
            .catch(this.handleError);
    }

    private toToken(res: Response) {
        return res.json().token;
    }

    isLogged() {
        let token = localStorage.getItem('x-auth-token');
        let name = this.getUsernameFromStorage();
        return token && name && token !== '' && name !== '';
    }

    getUsername() {
        let username = this.getUsernameFromStorage();
        return username ? username : '';
    }

    private getUsernameFromStorage() {
        return localStorage.getItem('x-auth-name');
    }

    getRole() {
        let role = localStorage.getItem('role');
        return role ? role : '';
    }

    private handleError(error: any) {
        console.error(error);
        return Observable.throw(
            new RestError(
                error.status,
                error.json().reason
            )
        );
    }
}