import {Component, Injectable, OnInit} from "@angular/core";
import {Router} from "@angular/router";
@Component({
    moduleId: module.id,
    selector: 'signout',
    templateUrl: 'signout.html'
})
@Injectable()
export class SignoutComponent implements OnInit{

    constructor(private router: Router) {

    }
    ngOnInit(): void {
        console.log('setting');
        localStorage.setItem('x-auth-token', '');
        localStorage.setItem('x-auth-name', '');
        localStorage.setItem('role', '');
        this.router.navigate(['wall']);
    }
}