import {Component, Injectable} from "@angular/core";
import {UserService} from "../service/user.service";
import {UserSignupRequestForm} from "./user-signup-request-form";
import {RestError} from "../../common/rest-error";
import {Router} from "@angular/router";
import {ERROR_MESSAGES} from "../../common/messages";


@Component({
    moduleId: module.id,
    selector: 'signup',
    templateUrl: 'signup.html'
})

@Injectable()
export class SignupComponent {
    signupRequest = new UserSignupRequestForm();
    errorMessage: string;

    constructor(private userService: UserService, private router: Router) {
    }

    signup(): void {
        this.userService.signup(this.signupRequest)
            .subscribe(
                user => {
                    this.errorMessage = "";
                    this.router.navigate(['signup-ok']);
                },
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    private mapErrorToMessage(error: RestError): string {
        if(error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}