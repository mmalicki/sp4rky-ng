import {Component, Injectable} from "@angular/core";

@Component({
    moduleId: module.id,
    selector: 'sparky-ok',
    templateUrl: 'signup-ok.html'
})

@Injectable()
export class SignupOkComponent {
}