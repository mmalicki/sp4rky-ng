export class UserSignupRequestForm {
    name: string;
    email: string;
    password: string;
    repeatedPassword: string;
}