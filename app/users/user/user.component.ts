import {Component, Injectable, OnInit} from "@angular/core";
import {Post} from "../../posts/model/post";
import {PostService} from "../../posts/service/post.service";
import {AuthService} from "../service/auth.service";
import {RestError} from "../../common/rest-error";
import {ActivatedRoute} from "@angular/router";
import {ERROR_MESSAGES} from "../../common/messages";

@Component({
    moduleId: module.id,
    selector: 'user-posts',
    templateUrl: 'user.html'
})

@Injectable()
export class UserPostsComponent implements OnInit {
    posts: Post[];
    comments: Comment[];
    errorMessage: string;
    userName: string;

    tabs = {
        comments: {isActive: true},
        posts: {isActive: false}
    };

    constructor(private postService: PostService, private route: ActivatedRoute, private authService: AuthService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => this.userName = params['name']);
        this.fetchAllPostsByUser(this.userName);
        this.fetchAllCommentsByUser(this.userName);
    }

    fetchAllPostsByUser(userName: string): void {
        this.postService.fetchAllByUser(userName)
            .subscribe(
                posts => this.posts = posts,
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    fetchAllCommentsByUser(userName: string): void {
        this.postService.fetchAllCommentsByUser(userName)
            .subscribe(
                comments => this.comments = comments,
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    isTabActive(tabName: string): boolean {
        return this.tabs[tabName].isActive;
    }

    activateTab(tabName: string): void {
        let t = this.tabs;
        Object.keys(t).map(function (tab) {
            t[tab].isActive = false;
        });
        this.tabs[tabName].isActive = true;
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}