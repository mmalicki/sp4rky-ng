import {Component, Injectable} from "@angular/core";
import {SigninRequest} from "./signin-request";
import {AuthService} from "../service/auth.service";
import {Router} from "@angular/router";
import {ERROR_MESSAGES} from "../../common/messages";
import {UserService} from "../service/user.service";

@Component({
    moduleId: module.id,
    selector: 'sparky-login',
    templateUrl : 'signin.html'
})
@Injectable()
export class SigninComponent {
    private signinRequest = new SigninRequest();
    private authService: AuthService;
    private userService: UserService;
    errorMessage: string;

    constructor(authService: AuthService, private router: Router, userService: UserService) {
        this.authService = authService;
        this.userService = userService;
    }

    signin() {
        this.userService.getUser(this.signinRequest.userName)
            .subscribe(
                result => localStorage.setItem('role', result.role),
                error => this.errorMessage = ERROR_MESSAGES[error.reason]
            );
        this.authService.signin(this.signinRequest)
            .subscribe(
                result => {
                    this.errorMessage = "";
                    localStorage.setItem('x-auth-token', result);
                    localStorage.setItem('x-auth-name', this.signinRequest.userName);

                    this.router.navigate(['wall']);
                },
                error => this.errorMessage = ERROR_MESSAGES[error.reason]
            );
    }
}