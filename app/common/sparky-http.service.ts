import {Http, Request, RequestOptionsArgs, Response, XHRBackend, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";

@Injectable()
export class SparkyHttpService extends Http{

    constructor (backend: XHRBackend, options: RequestOptions) {
        super(backend, options);
    }

    request(url: Request, options?: RequestOptionsArgs): Observable<Response> {
        url.headers.set('Content-Type', 'application/json');
        url.headers.set('x-auth-token', localStorage.getItem('x-auth-token'));
        url.headers.set('x-auth-name', localStorage.getItem('x-auth-name'));
        return super.request(url, options).catch(this.catchAuthError(this));
    }

    private catchAuthError (self: SparkyHttpService) {
        return (res: Response) => {
            console.log(res);
            if (res.status === 401 || res.status === 403) {
                console.log(res);
            }
            return Observable.throw(res);
        };
    }
}