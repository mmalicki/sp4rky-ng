export const ERROR_MESSAGES = {
     "MESSAGE_NOT_APPROPRIATE" : "Wiadomość zawiera niedozwolona słowa",
    "NAME_EMPTY" : "Pole nazwy użytkownika jest puste",
    "UNAUTHORIZED" : "Nie masz odpowiednich uprawnień do wykonania tej akcji",
    "PASSWORDS_NOT_EQUAL" : "Hasła się różnią",
    "PASSWORD_EMPTY" : "Pole hasła jest puste",
    "EMAIL_TAKEN" : "Podany email jest już zajęty",
    "NAME_TAKEN" : "Podana nazwa użytkownika jest już zajęta",
    "USER_NOT_FOUND" : "Nie znaleziono danego użytkownika",
    "EMAIL_EMPTY" : "Pole email jest puste",
    "PASSWORDS_DIFFERENT" : "Hasła się różnią",
    "WRONG_CREDENTIALS" : "Niepoprawny login lub hasło"
};