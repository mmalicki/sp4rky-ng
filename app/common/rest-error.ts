export class RestError {
    private _httpCode: number;
    private _reason: string;

    constructor(httpCode: number, reason: string) {
        this._httpCode = httpCode;
        this._reason = reason;
    }

    get httpCode(): number {
        return this._httpCode;
    }

    get reason(): string {
        return this._reason;
    }
}