

import {Component, Injectable, OnInit, Input, Output, EventEmitter} from "@angular/core";
import {EditCommentRequest} from "./edit-comment-request";
import {PostService} from "../../posts/service/post.service";
import {Router} from "@angular/router";
import {RestError} from "../../common/rest-error";
import {ERROR_MESSAGES} from "../../common/messages";
import {Comment} from "../comment";

@Component({
    moduleId: module.id,
    selector: 'edit-comment-form',
    templateUrl: 'edit-comment-form.html'
})

@Injectable()
export class EditCommentComponent implements OnInit{

    editCommentRequest = new EditCommentRequest();
    errorMessage: string;

    @Input()
    comment: Comment;

    @Output() onEditedComment = new EventEmitter<string>();

    constructor(private postService: PostService, private router: Router) {
    }


    ngOnInit(): void {
        this.editCommentRequest.message = this.comment.message;
    }

    editComment(): void {
        this.postService.editComment(this.comment.id, this.editCommentRequest)
            .subscribe(
                comment => {
                    this.onEditedComment.emit("editedComment");
                    this.router.navigate(['/posts/' + this.comment.postId]);
                },
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}