import {Component, Injectable, Input, OnChanges} from "@angular/core";
import {PostService} from "../../posts/service/post.service";
import {Comment} from "../comment";
import {Post} from "../../posts/model/post";
import {RestError} from "../../common/rest-error";
import {ActivatedRoute, Route} from "@angular/router";
import {User} from "../../users/model/user";
import {ERROR_MESSAGES} from "../../common/messages";
import {AuthService} from "../../users/service/auth.service";

@Component({
    moduleId: module.id,
    selector: 'comments-list',
    templateUrl: 'comments-list.html'
})

@Injectable()
export class CommentsListComponent implements OnChanges {
    comments: Comment[];
    errorMessage: string;
    @Input()
    post: Post;
    @Input()
    user: User;

    route: ActivatedRoute;
    authService: AuthService;


    editedComment: Comment;

    onEditedComment(n: boolean) {
        this.editedComment = null;
        this.route.params.subscribe(params => this.fetch(params['id']));
    }

    fetch(postToFetchId: number): void {
        this.postService.fetchAllComments(postToFetchId)
            .subscribe(
                comments => this.comments = comments,
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    isAllowedToEdit(comment: Comment) {
        return this.authService.getUsername() === comment.user.name || this.authService.getRole() === "ADMIN";
    }

    isInEditMode(comment: Comment) {
        return this.editedComment === comment;
    }

    markForEdit(comment: Comment) {
        this.editedComment = comment;
    }

    constructor(private postService: PostService, route: ActivatedRoute,authService : AuthService) {
        this.route = route;
        this.authService = authService;
    }

    ngOnChanges(changes) {
        if (this.post !== undefined) {
            this.fetchAll();
        }
        if(this.user !== undefined) {
            this.fetchAllByUser();
        }
    }

    fetchAll(): void {
        this.postService.fetchAllComments(this.post.id)
            .subscribe(
                comments => this.comments = comments,
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    fetchAllByUser(): void {
        this.postService.fetchAllCommentsByUser(this.user.name)
            .subscribe(
                comments => this.comments = comments,
                error => this.errorMessage = this.mapErrorToMessage(error)
            );
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}