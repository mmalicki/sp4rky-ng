import {Component, Injectable, Input, ChangeDetectorRef, OnInit, Output, EventEmitter} from "@angular/core";
import {RestError} from "../../common/rest-error";
import {Router} from "@angular/router";
import {PostService} from "../../posts/service/post.service";
import {NewCommentRequest} from "./new-comment-request";
import {Post} from "../../posts/model/post";
import {ERROR_MESSAGES} from "../../common/messages";


@Component({
    moduleId: module.id,
    selector: 'new-comment-form',
    templateUrl: 'new-comment-form.html'
})

@Injectable()
export class NewCommentComponent {
    newCommentRequest = new NewCommentRequest();
    errorMessage: string;
    @Input()
    post: Post;

    @Output() onNewComment = new EventEmitter<boolean>();

    constructor(private postService: PostService, private router: Router) {
    }

    createComment(): void {
        if (this.post !== undefined) {
            this.postService.createComment(this.post.id, this.newCommentRequest)
                .subscribe(
                    comment => {
                        this.newCommentRequest.message = '';
                        this.onNewComment.emit(true);
                        this.router.navigate(['posts/' + this.post.id]);
                    },
                    error => this.errorMessage = this.mapErrorToMessage(error)
                );
        } else {
            alert("Not ready yet.");
        }
    }

    private mapErrorToMessage(error: RestError): string {
        if (error.httpCode === 0) {
            return "NO CONNECTION TO SPARKY";
        }
        return ERROR_MESSAGES[error.reason];
    }
}