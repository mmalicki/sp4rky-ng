import {User} from "../users/model/user";
export class Comment {
    constructor(readonly id: number, readonly postId: number, readonly user: User, readonly message: string, readonly postedAt: string, readonly imagePath: string) {
    }

    static createFrom(commentAsJson: any) {
        return new Comment(
            commentAsJson.id,
            commentAsJson.postId,
            User.createFrom(commentAsJson.user),
            commentAsJson.message,
            commentAsJson.postedAt,
            !commentAsJson.imagePath ? 'assets/no-image.png' : commentAsJson.imagePath
        );
    }
}